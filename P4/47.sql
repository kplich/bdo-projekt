CREATE OR REPLACE TYPE Kocur AS OBJECT(
    imie VARCHAR2(15),
    plec VARCHAR2(1),
    pseudo VARCHAR2(15),
    funkcja VARCHAR2(10),
    szef REF Kocur,
    w_stadku_od Date,
    przydzial_myszy number(3),
    myszy_extra number(3),
    nr_bandy number(2),
    MAP MEMBER FUNCTION mapujKocura RETURN VARCHAR2,
    MEMBER FUNCTION ileLatWStadku RETURN NUMBER,
    MEMBER FUNCTION calkowityPrzydzial RETURN NUMBER,
    MEMBER FUNCTION dajSzefa RETURN Kocur);

CREATE OR REPLACE TYPE BODY Kocur AS
    MAP MEMBER FUNCTION mapujKocura RETURN VARCHAR2 IS
    BEGIN
        RETURN imie || plec || pseudo || funkcja;
    end;

    MEMBER FUNCTION ileLatWStadku RETURN NUMBER IS
    BEGIN
        RETURN FLOOR(MONTHS_BETWEEN(SYSDATE, w_stadku_od) / 12);
    end;
    MEMBER FUNCTION calkowityPrzydzial RETURN NUMBER IS
    BEGIN
        return NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0);
    end;
    MEMBER FUNCTION dajSzefa RETURN Kocur IS
        wynik Kocur;
    BEGIN
        SELECT DEREF(SELF.SZEF) INTO wynik FROM DUAL;
        return wynik;
    end;
END;

CREATE OR REPLACE TYPE Plebs AS OBJECT(
    id_plebsu number(3),
    ref_kocur ref Kocur,
    MAP MEMBER FUNCTION mapujPlebs RETURN VARCHAR2,
    MEMBER FUNCTION dajPseudo RETURN VARCHAR2);

CREATE OR REPLACE TYPE BODY Plebs AS
    MAP MEMBER FUNCTION mapujPlebs RETURN VARCHAR2 IS
        wynik varchar2(500);
    BEGIN
        SELECT DEREF(ref_kocur).MAPUJKOCURA() INTO wynik FROM Dual;
        return wynik;
    end;

    MEMBER FUNCTION dajPseudo RETURN VARCHAR2 IS
        wynik varchar2(500);
    BEGIN
        SELECT DEREF(ref_kocur).PSEUDO INTO wynik FROM Dual;
        return wynik;
    end;
END;

CREATE OR REPLACE TYPE Elita AS OBJECT(
    id_elity number(3),
    ref_kocur ref Kocur,
    ref_plebs REF Plebs,
    MAP MEMBER FUNCTION mapujElite RETURN VARCHAR2,
    MEMBER FUNCTION dajPseudoElity RETURN VARCHAR2,
    MEMBER FUNCTION dajPseudoSlugi RETURN VARCHAR2);

CREATE OR REPLACE TYPE BODY Elita AS
    MAP MEMBER FUNCTION mapujElite RETURN VARCHAR2 IS
        wynik VARCHAR2(500);
    BEGIN
        SELECT DEREF(ref_kocur).mapujKocura() INTO wynik FROM Dual;
        return wynik;
    end;

    MEMBER FUNCTION dajPseudoElity RETURN VARCHAR2 IS
        wynik VARCHAR2(500);
    BEGIN
        SELECT DEREF(ref_kocur).pseudo INTO wynik FROM Dual;
    end;

    MEMBER FUNCTION dajPseudoSlugi RETURN VARCHAR2 IS
        wynik VARCHAR2(500);
    BEGIN
        SELECT DEREF(DEREF(ref_plebs).ref_kocur).pseudo INTO wynik From Dual;
    end;
END;

CREATE OR REPLACE TYPE Wpis_na_konto AS OBJECT(
    id_wpisu number(3),
    data_wprowadzenia Date,
    data_usuniecia Date,
    wlasciciel ref Elita,
    MEMBER FUNCTION pseudoWlasciciela RETURN VARCHAR2,
    MAP MEMBER FUNCTION mapujWpis RETURN NUMBER);

CREATE OR REPLACE TYPE BODY Wpis_na_konto AS
    MAP MEMBER FUNCTION mapujWpis RETURN NUMBER IS
    BEGIN
        RETURN id_wpisu;
    end;

    MEMBER FUNCTION pseudoWlasciciela RETURN VARCHAR2 IS
        wynik VARCHAR2(50);
    BEGIN
        SELECT DEREF(DEREF(WLASCICIEL).ref_kocur).pseudo INTO wynik FROM Dual;
    end;
END;

CREATE OR REPLACE TYPE Incydent AS OBJECT(
    imie_wroga VARCHAR2(15),
    poszkodowany ref Kocur,
    data_incydentu DATE,
    opis_incydentu VARCHAR2(50)
);

CREATE OR REPLACE TRIGGER pseudoWrogaJedenRaz
BEFORE INSERT
ON Incydenty_O
FOR EACH ROW
DECLARE
    liczba NUMBER(3);
    wrog INCYDENTY_O.imie_wroga%TYPE;
    kot INCYDENTY_O.POSZKODOWANY%TYPE;
BEGIN
    wrog := :NEW.imie_wroga;
    kot := :NEW.poszkodowany;
    SELECT COUNT(*) INTO liczba FROM INCYDENTY_O inc WHERE inc.imie_wroga = wrog AND inc.POSZKODOWANY = kot;
    IF(liczba>0) THEN  RAISE_APPLICATION_ERROR(-20001, 'Kot o tym pseudo ma juz takiego wroga');
    END IF;
END;

CREATE TABLE Incydenty_O OF Incydent(
    CONSTRAINT i_o_fk FOREIGN KEY (imie_wroga) REFERENCES WROGOWIE(imie_wroga),
    poszkodowany SCOPE IS Kocury_O
);

CREATE TABLE Kocury_O OF Kocur(
    CONSTRAINT k_o_pk PRIMARY KEY (pseudo),
    CONSTRAINT k_o_imie_nn CHECK(imie IS NOT NULL),
    CONSTRAINT k_o_plec_dm CHECK(plec IN ('D', 'M')),
    szef SCOPE IS Kocury_O
);

CREATE TABLE Plebsy_O OF Plebs(
    CONSTRAINT plebsy_pk PRIMARY KEY (id_plebsu),
    ref_kocur SCOPE IS Kocury_O
);

CREATE TABLE Elity_O OF Elita(
    CONSTRAINT elity_pk PRIMARY KEY (id_elity),
    ref_kocur SCOPE IS Kocury_O,
    ref_plebs SCOPE IS Plebsy_O
);

CREATE TABLE Konto_O OF Wpis_na_konto(
    CONSTRAINT konto_pk PRIMARY KEY (id_wpisu),
    wlasciciel SCOPE IS Elity_O
);

INSERT INTO Kocury_O VALUES (Kocur('MRUCZEK','M','TYGRYS','SZEFUNIO',NULL,'2002-01-01',103,33,1));
INSERT INTO Kocury_O VALUES (Kocur('MICKA','D','LOLA','MILUSIA',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='TYGRYS'),'2009-10-14',25,47,1));
INSERT INTO Kocury_O VALUES (Kocur('CHYTRY','M','BOLEK','DZIELCZY',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='TYGRYS'),'2002-05-05',50,NULL,1));
INSERT INTO Kocury_O VALUES (Kocur('KOREK','M','ZOMBI','BANDZIOR',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='TYGRYS'),'2004-03-16',75,13,3));
INSERT INTO Kocury_O VALUES (Kocur('BOLEK','M','LYSY','BANDZIOR',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='TYGRYS'),'2006-08-15',72,21,2));
INSERT INTO Kocury_O VALUES (Kocur('RUDA','D','MALA','MILUSIA',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='TYGRYS'),'2006-09-17',22,42,1));
INSERT INTO Kocury_O VALUES (Kocur('PUCEK','M','RAFA','LOWCZY',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='TYGRYS'),'2006-10-15',65,NULL,4));
INSERT INTO Kocury_O VALUES (Kocur('PUNIA','D','KURKA','LOWCZY',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='ZOMBI'),'2008-01-01',61,NULL,3));
INSERT INTO Kocury_O VALUES (Kocur('JACEK','M','PLACEK','LOWCZY',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='LYSY'),'2008-12-01',67,NULL,2));
INSERT INTO Kocury_O VALUES (Kocur('BARI','M','RURA','LAPACZ',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='LYSY'),'2009-09-01',56,NULL,2));
INSERT INTO Kocury_O VALUES (Kocur('LUCEK','M','ZERO','KOT',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='KURKA'),'2010-03-01',43,NULL,3));
INSERT INTO Kocury_O VALUES (Kocur('SONIA','D','PUSZYSTA','MILUSIA',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='ZOMBI'),'2010-11-18',20,35,3));
INSERT INTO Kocury_O VALUES (Kocur('LATKA','D','UCHO','KOT',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='RAFA'),'2011-01-01',40,NULL,4));
INSERT INTO Kocury_O VALUES (Kocur('DUDEK','M','MALY','KOT',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='RAFA'),'2011-05-15',40,NULL,4));
INSERT INTO Kocury_O VALUES (Kocur('ZUZIA','D','SZYBKA','LOWCZY',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='LYSY'),'2006-07-21',65,NULL,2));
INSERT INTO Kocury_O VALUES (Kocur('BELA','D','LASKA','MILUSIA',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='LYSY'),'2008-02-01',24,28,2));
INSERT INTO Kocury_O VALUES (Kocur('KSAWERY','M','MAN','LAPACZ',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='RAFA'),'2008-07-12',51,NULL,4));
INSERT INTO Kocury_O VALUES (Kocur('MELA','D','DAMA','LAPACZ',(SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='RAFA'),'2008-11-01',51,NULL,4));

INSERT INTO Incydenty_O VALUES('KAZIO', (SELECT REF(kocur) FROM Kocury_o kocur WHERE kocur.pseudo='TYGRYS'), '2020-01-20', 'incydent');

INSERT INTO Plebsy_O VALUES (Plebs(1, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='SZYBKA')));
INSERT INTO Plebsy_O VALUES (Plebs(2, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='KURKA')));
INSERT INTO Plebsy_O VALUES (Plebs(3, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='MAN')));
INSERT INTO Plebsy_O VALUES (Plebs(4, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='DAMA')));
INSERT INTO Plebsy_O VALUES (Plebs(5, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='MALA')));
INSERT INTO Plebsy_O VALUES (Plebs(6, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='PLACEK')));
INSERT INTO Plebsy_O VALUES (Plebs(7, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='RURA')));
INSERT INTO Plebsy_O VALUES (Plebs(8, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='ZERO')));
INSERT INTO Plebsy_O VALUES (Plebs(9, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='PUSZYSTA')));
INSERT INTO Plebsy_O VALUES (Plebs(10, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='UCHO')));
INSERT INTO Plebsy_O VALUES (Plebs(11, (SELECT REF(kocur) FROM Kocury_O kocur WHERE kocur.pseudo='MALY')));

INSERT INTO Elity_O VALUES (Elita(1, (SELECT REF(kot) FROM Kocury_O kot WHERE kot.pseudo='TYGRYS'), (SELECT REF(sluga) FROM Plebsy_O sluga WHERE sluga.ref_kocur.pseudo = 'UCHO')));
INSERT INTO Elity_O VALUES (Elita(2, (SELECT REF(kot) FROM Kocury_O kot WHERE kot.pseudo='LOLA'), (SELECT REF(sluga) FROM Plebsy_O sluga WHERE sluga.ref_kocur.pseudo = 'MALA')));
INSERT INTO Elity_O VALUES (Elita(3, (SELECT REF(kot) FROM Kocury_O kot WHERE kot.pseudo='ZOMBI'), (SELECT REF(sluga) FROM Plebsy_O sluga WHERE sluga.ref_kocur.pseudo = 'RURA')));
INSERT INTO Elity_O VALUES (Elita(4, (SELECT REF(kot) FROM Kocury_O kot WHERE kot.pseudo='LYSY'), (SELECT REF(sluga) FROM Plebsy_O sluga WHERE sluga.ref_kocur.pseudo = 'ZERO')));
INSERT INTO Elity_O VALUES (Elita(5, (SELECT REF(kot) FROM Kocury_O kot WHERE kot.pseudo='BOLEK'), (SELECT REF(sluga) FROM Plebsy_O sluga WHERE sluga.ref_kocur.pseudo = 'PUSZYSTA')));
INSERT INTO Elity_O VALUES (Elita(6, (SELECT REF(kot) FROM Kocury_O kot WHERE kot.pseudo='RAFA'), (SELECT REF(sluga) FROM Plebsy_O sluga WHERE sluga.ref_kocur.pseudo = 'MALY')));
INSERT INTO Elity_O VALUES (Elita(7, (SELECT REF(kot) FROM Kocury_O kot WHERE kot.pseudo='LASKA'), (SELECT REF(sluga) FROM Plebsy_O sluga WHERE sluga.ref_kocur.pseudo = 'SZYBKA')));

INSERT INTO Konto_O VALUES (1,  '2017-12-01', '2017-12-03', (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=1));
INSERT INTO Konto_O VALUES (2,  '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=2));
INSERT INTO Konto_O VALUES (3,  '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=1));
INSERT INTO Konto_O VALUES (4,  '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=1));
INSERT INTO Konto_O VALUES (5,  '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=5));
INSERT INTO Konto_O VALUES (6,  '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=6));
INSERT INTO Konto_O VALUES (7,  '2017-12-01', SYSDATE, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=2));
INSERT INTO Konto_O VALUES (8,  '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=3));
INSERT INTO Konto_O VALUES (9,  '2017-12-01', SYSDATE, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=2));
INSERT INTO Konto_O VALUES (10, '2017-12-01', SYSDATE, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=7));
INSERT INTO Konto_O VALUES (11, '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=2));
INSERT INTO Konto_O VALUES (12, '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=2));
INSERT INTO Konto_O VALUES (13, '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=3));
INSERT INTO Konto_O VALUES (14, '2017-12-01', '2017-12-03', (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=7));
INSERT INTO Konto_O VALUES (15, '2017-12-01', SYSDATE, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=7));
INSERT INTO Konto_O VALUES (16, '2017-12-01', SYSDATE, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=2));
INSERT INTO Konto_O VALUES (17, '2017-12-01', SYSDATE, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=2));
INSERT INTO Konto_O VALUES (18, '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=1));
INSERT INTO Konto_O VALUES (19, '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=1));
INSERT INTO Konto_O VALUES (20, '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=5));
INSERT INTO Konto_O VALUES (21, '2017-12-01', NULL, (SELECT REF(kot) FROM Elity_O kot WHERE kot.id_elity=2));

-- grupowanie i referencja
SELECT K_O.SZEF.PSEUDO szef, COUNT(K_O.PSEUDO)
FROM KOCURY_O K_O
GROUP BY K_O.SZEF.PSEUDO;

-- podzapytanie i funkcja zdefiniowana na obiekcie
SELECT K_O.PSEUDO pseudo, K_O.calkowityPrzydzial() przydzial
FROM KOCURY_O K_O
WHERE K_O.calkowityPrzydzial() > (SELECT AVG(K_O2.calkowityPrzydzial())
                               FROM Kocury_O K_O2);

-- zadanie 18
SELECT K1.imie, K1.w_stadku_od
FROM Kocury_O K1 JOIN Kocury_O K2
                 ON K1.w_stadku_od < K2.w_stadku_od AND K2.imie = 'JACEK'
ORDER BY K1.w_stadku_od DESC;

-- zadanie 22 --nie wykorzystywac joina
SELECT pseudo, PO.REF_KOCUR.funkcja funkcja, COUNT(IMIE_WROGA) ilosc_wrogow
FROM ELITY_O PO JOIN Wrogowie_kocurow WK ON PO.REF_KOCUR.PSEUDO = WK.PSEUDO
GROUP BY pseudo, PO.REF_KOCUR.funkcja
    HAVING COUNT(IMIE_WROGA) > 1;

-- zadanie 37, funkcje zdefiniowane na obiekcie, podzapytanie
BEGIN
    FOR kot IN (SELECT ROWNUM numer, pseudo, myszy_calk
                FROM (
                    SELECT KO.PSEUDO, KO.calkowityPrzydzial() myszy_calk
                    FROM KOCURY_O KO
                    ORDER BY myszy_calk DESC)
                WHERE ROWNUM <= 5) LOOP
        DBMS_OUTPUT.PUT_LINE(kot.numer || ' ' || kot.pseudo || ' ' || kot.myszy_calk);
    END LOOP;
END;

-- zadanie 41
DECLARE
    funkcja_kota KOCURY_O.FUNKCJA%TYPE := 'KOT';
    liczba_kotow NUMBER;
BEGIN
    SELECT COUNT(KO.pseudo)
    INTO liczba_kotow
    FROM KOCURY_O KO
    WHERE KO.FUNKCJA = funkcja_kota;

    IF liczba_kotow > 0 THEN
        DBMS_OUTPUT.PUT_LINE('Znaleziono koty pelniace funkcje ' || funkcja_kota);
    ELSE
        DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota pelniacego funkcje ' || funkcja_kota);
    END IF;
END;

    --koty z elity plci meskiej, ich sluge i liczbe myszy dostepnych na koncie

SELECT E_O.REF_KOCUR.PSEUDO elita,
       E_O.REF_PLEBS.REF_KOCUR.PSEUDO sluga,
       (SELECT COUNT(*) FROM Konto_O K_O WHERE K_O.WLASCICIEL.REF_KOCUR.PSEUDO = E_O.REF_KOCUR.PSEUDO) myszy
FROM ELITY_O E_O
WHERE E_O.REF_KOCUR.PLEC = 'M';

SELECT E_O.REF_KOCUR.PSEUDO elita,
       E_O.REF_PLEBS.REF_KOCUR.PSEUDO sluga,
       (SELECT COUNT(*) FROM Konto_V K_O WHERE K_O.WLASCICIEL.REF_KOCUR.PSEUDO = E_O.REF_KOCUR.PSEUDO) myszy
FROM ELITY_V E_O
WHERE E_O.REF_KOCUR.PLEC = 'M';

-- zadanie 22 --nie wykorzystywac joina
SELECT PO.REF_KOCUR.PSEUDO pseudo, PO.REF_KOCUR.funkcja funkcja, (SELECT COUNT(*) FROM WROGOWIE_KOCUROW WHERE PSEUDO = PO.REF_KOCUR.PSEUDO) ilosc_wrogow
FROM ELITY_O PO
GROUP BY  PO.REF_KOCUR.PSEUDO, PO.REF_KOCUR.funkcja
    HAVING (SELECT COUNT(*) FROM WROGOWIE_KOCUROW WHERE PSEUDO = PO.REF_KOCUR.PSEUDO) > 1;