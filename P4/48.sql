
-- zad 48

-- tablice
create table Plebsy
(
   id_plebsu number
       constraint pl_pk primary key,
   kocur    varchar2(15)
       constraint pl_koc_fk references KOCURY (pseudo)
);
create table Elity
(
   id_elity number
       constraint el_pk primary key,
   kocur    varchar2(15)
       constraint el_koc_fk references KOCURY (pseudo),
   sluga    number(15)
       constraint el_slug_fk references Plebsy (id_plebsu)
);
create table Konto
(
   id_wpisu          integer
       constraint kon_pk primary key,
   wlasciciel        integer
       constraint kon_wl_fk references Elity (id_elity),
   data_wprowadzenia date,
   data_usuniecia    date
);

-- drop tablic
drop table Plebsy;
drop table Elity;
drop table Konto;

-- insert data
INSERT INTO Plebsy VALUES (1,'SZYBKA');
INSERT INTO Plebsy VALUES (2,'KURKA');
INSERT INTO Plebsy VALUES (3,'MAN');
INSERT INTO Plebsy VALUES (4,'DAMA');
INSERT INTO Plebsy VALUES (5,'MALA');
INSERT INTO Plebsy VALUES (6,'PLACEK');
INSERT INTO Plebsy VALUES (7,'RURA');
INSERT INTO Plebsy VALUES (8,'ZERO');
INSERT INTO Plebsy VALUES (9,'PUSZYSTA');
INSERT INTO Plebsy VALUES (10,'UCHO');
INSERT INTO Plebsy VALUES (11,'MALY');
commit;

INSERT INTO Elity VALUES (1, 'TYGRYS', 10);
INSERT INTO Elity VALUES (2, 'LOLA', 5);
INSERT INTO Elity VALUES (3, 'ZOMBI', 7);
INSERT INTO Elity VALUES (4, 'LYSY', 8);
INSERT INTO Elity VALUES (5, 'BOLEK', 9);
INSERT INTO Elity VALUES (6, 'RAFA', 11);
INSERT INTO Elity VALUES (7, 'LASKA', 1);
commit;

INSERT INTO Konto VALUES (1, 1, '2019-12-03', '2019-12-05');
INSERT INTO Konto VALUES (2, 2, '2019-05-05', NULL);
INSERT INTO Konto VALUES (3, 3, '2019-07-05', SYSDATE);
INSERT INTO Konto VALUES (4, 4, '2019-08-23', NULL);
INSERT INTO Konto VALUES (5, 5, '2019-12-13', NULL);
INSERT INTO Konto VALUES (6, 6, '2019-12-05', SYSDATE);
INSERT INTO Konto VALUES (7, 2, '2019-09-05', NULL);
INSERT INTO Konto VALUES (8, 3, '2019-10-05', NULL);
INSERT INTO Konto VALUES (9, 2, '2019-11-05', SYSDATE);
INSERT INTO Konto VALUES (10, 5, '2019-11-15', NULL);
INSERT INTO Konto VALUES (11, 2, '2019-12-15', NULL);
commit;

-- perspektywy
CREATE OR REPLACE FORCE VIEW Kocury_V OF KOCUR
WITH OBJECT IDENTIFIER (pseudo) AS
SELECT imie, plec, pseudo, funkcja, MAKE_REF(Kocury_V, szef) szef,
       w_stadku_od, przydzial_myszy, myszy_extra, nr_bandy
FROM Kocury;

CREATE OR REPLACE VIEW Plebsy_V OF PLEBS
WITH OBJECT IDENTIFIER (id_plebsu) AS
SELECT id_plebsu, MAKE_REF(Kocury_V, kocur) ref_kocur
FROM Plebsy;

CREATE OR REPLACE VIEW Elity_V OF ELITA
WITH OBJECT IDENTIFIER (id_elity) AS
SELECT
  id_elity, MAKE_REF(Kocury_V, kocur) ref_kocur, MAKE_REF(Plebsy_V, sluga) ref_plebs
FROM Elity;

CREATE OR REPLACE VIEW Konto_V OF WPIS_NA_KONTO
WITH OBJECT IDENTIFIER (id_wpisu) AS
SELECT
  id_wpisu, data_wprowadzenia, data_usuniecia, MAKE_REF(Elity_V, wlasciciel) wlasciciel
FROM Konto;

/*CREATE OR REPLACE VIEW INCYDENTY_VIEW OF INCYDENT_TYPE
WITH OBJECT IDENTIFIER (incydent_id) AS
SELECT
   incydent_id,
   kot,
   imie_wroga,
   data_incydentu,
   opis_incydentu
FROM INCYDENTY_O;*/

-- grupowanie i referencja
SELECT K_O.SZEF.PSEUDO szef, COUNT(K_O.PSEUDO)
FROM Kocury_V K_O
GROUP BY K_O.SZEF.PSEUDO;

-- podzapytanie i funkcja zdefiniowana na obiekcie
SELECT K_O.PSEUDO pseudo, K_O.calkowityPrzydzial() przydzial
FROM Kocury_V K_O
WHERE K_O.calkowityPrzydzial() > (SELECT AVG(K_O2.calkowityPrzydzial())
                               FROM Kocury_O K_O2);

-- zadanie 18
SELECT K1.imie, K1.w_stadku_od
FROM Kocury_V K1 JOIN Kocury_V K2
                 ON K1.w_stadku_od < K2.w_stadku_od AND K2.imie = 'JACEK'
ORDER BY K1.w_stadku_od DESC;

-- zadanie 22 --nie wykorzystywac joina
SELECT PO.REF_KOCUR.PSEUDO pseudo, PO.REF_KOCUR.funkcja funkcja, (SELECT COUNT(*) FROM WROGOWIE_KOCUROW WHERE PSEUDO = PO.REF_KOCUR.PSEUDO) ilosc_wrogow
FROM ELITY_V PO
GROUP BY  PO.REF_KOCUR.PSEUDO, PO.REF_KOCUR.funkcja
    HAVING (SELECT COUNT(*) FROM WROGOWIE_KOCUROW WHERE PSEUDO = PO.REF_KOCUR.PSEUDO) > 1;

-- zadanie 37, funkcje zdefiniowane na obiekcie, podzapytanie
BEGIN
    FOR kot IN (SELECT ROWNUM numer, pseudo, myszy_calk
                FROM (
                    SELECT KO.PSEUDO, KO.calkowityPrzydzial() myszy_calk
                    FROM KOCURY_V KO
                    ORDER BY myszy_calk DESC)
                WHERE ROWNUM <= 5) LOOP
        DBMS_OUTPUT.PUT_LINE(kot.numer || ' ' || kot.pseudo || ' ' || kot.myszy_calk);
    END LOOP;
END;

-- zadanie 41
DECLARE
    funkcja_kota KOCURY_O.FUNKCJA%TYPE := &funkcja;
    liczba_kotow NUMBER;
BEGIN
    SELECT COUNT(KO.pseudo)
    INTO liczba_kotow
    FROM Kocury_V KO
    WHERE KO.FUNKCJA = funkcja_kota;

    IF liczba_kotow > 0 THEN
        DBMS_OUTPUT.PUT_LINE('Znaleziono koty pelniace funkcje ' || funkcja_kota);
    ELSE
        DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota pelniacego funkcje ' || funkcja_kota);
    END IF;
END;