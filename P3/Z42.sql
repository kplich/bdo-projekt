--zad 42
CREATE OR REPLACE PACKAGE milusiowyWirus AS
    przydzialMyszyTygrysa NUMBER(5) := 0;
    karaDlaTygrysa NUMBER(5) := 0;
    nagrodaDlaTygrysa NUMBER(5) := 0;
    done BOOLEAN := false;
    PROCEDURE wczytajPrzydzialTygrysa;
    PROCEDURE karaNagrodaDlaTygrysa;
END;

CREATE OR REPLACE PACKAGE BODY milusiowyWirus AS

    PROCEDURE wczytajPrzydzialTygrysa IS
    BEGIN
        SELECT NVL(PRZYDZIAL_MYSZY, 0)
        INTO przydzialMyszyTygrysa
        FROM KOCURY
        WHERE PSEUDO = 'TYGRYS';
    end wczytajPrzydzialTygrysa;

    PROCEDURE karaNagrodaDlaTygrysa IS
    BEGIN
        DBMS_OUTPUT.PUT_LINE('begin procedure kara/nagroda');
        IF (NOT done) THEN
            done := true;
            DBMS_OUTPUT.PUT_LINE('begin update tygrysa');
            UPDATE KOCURY
            SET PRZYDZIAL_MYSZY = PRZYDZIAL_MYSZY - karaDlaTygrysa,
                MYSZY_EXTRA     = MYSZY_EXTRA + nagrodaDlaTygrysa
            WHERE PSEUDO = 'TYGRYS';
            DBMS_OUTPUT.PUT_LINE('end update tygrysa');
        ELSE
            karaDlaTygrysa := 0;
            nagrodaDlaTygrysa := 0;
        END IF;
    END karaNagrodaDlaTygrysa;
END milusiowyWirus;

CREATE OR REPLACE TRIGGER wczytajPrzydzialTygrysa
    BEFORE UPDATE
    ON KOCURY
BEGIN
    milusiowyWirus.wczytajPrzydzialTygrysa();
    DBMS_OUTPUT.PUT_LINE('wczytano przydzial tygrysa');
end;

CREATE OR REPLACE TRIGGER utulZalMilusich
    BEFORE UPDATE
    ON KOCURY
    FOR EACH ROW
    WHEN ( OLD.FUNKCJA = 'MILUSIA' )
DECLARE
    roznicaWPrzydziale NUMBER(3) := 0;
BEGIN
    DBMS_OUTPUT.PUT_LINE('utulam zal milusi');
    roznicaWPrzydziale := :NEW.przydzial_myszy - :OLD.przydzial_myszy;

    -- jesli nastepuje proba zmniejszenia przydzialu, zablokuj ja
    IF (roznicaWPrzydziale < 0) THEN
        :NEW.przydzial_myszy := :OLD.przydzial_myszy;
    end if;

    IF (roznicaWPrzydziale > 0) THEN
        IF (roznicaWPrzydziale < ROUND(0.1 * milusiowyWirus.przydzialMyszyTygrysa)) THEN
            DBMS_OUTPUT.PUT_LINE('before changes');
            DBMS_OUTPUT.PUT_LINE(
                        'old: ' || :old.IMIE || ', pm: ' || :old.PRZYDZIAL_MYSZY || ', me: ' || :old.MYSZY_EXTRA);
            DBMS_OUTPUT.PUT_LINE(
                        'new: ' || :NEW.IMIE || ', pm: ' || :new.PRZYDZIAL_MYSZY || ', me: ' || :new.MYSZY_EXTRA);
            :NEW.przydzial_myszy := :OLD.przydzial_myszy + ROUND(0.1 * milusiowyWirus.przydzialMyszyTygrysa);
            :NEW.myszy_extra := :NEW.myszy_extra + 5;

            DBMS_OUTPUT.PUT_LINE('after changes');
            DBMS_OUTPUT.PUT_LINE(
                        'old: ' || :old.IMIE || ', pm: ' || :old.PRZYDZIAL_MYSZY || ', me: ' || :old.MYSZY_EXTRA);
            DBMS_OUTPUT.PUT_LINE(
                        'new: ' || :NEW.IMIE || ', pm: ' || :new.PRZYDZIAL_MYSZY || ', me: ' || :new.MYSZY_EXTRA);

            milusiowyWirus.karaDlaTygrysa := milusiowyWirus.karaDlaTygrysa + 0.1 * milusiowyWirus.przydzialMyszyTygrysa;
        ELSE
            milusiowyWirus.nagrodaDlaTygrysa := milusiowyWirus.nagrodaDlaTygrysa + 5;
        END IF;
    end if;
    DBMS_OUTPUT.PUT_LINE('skonczylem utulac zal milusi');
end;

CREATE OR REPLACE TRIGGER ukarzNagrodzTygrysa
    AFTER UPDATE
    ON KOCURY
BEGIN
    DBMS_OUTPUT.PUT_LINE('begin trigger nagroda tygrysa');
    DBMS_OUTPUT.PUT_LINE('kara: ' || milusiowyWirus.karaDlaTygrysa);
    DBMS_OUTPUT.PUT_LINE('nagroda: ' || milusiowyWirus.nagrodaDlaTygrysa);

    milusiowyWirus.karaNagrodaDlaTygrysa();
    milusiowyWirus.done := false;
    DBMS_OUTPUT.PUT_LINE('end trigger nagroda tygrysa');
end;

SELECT *
FROM KOCURY;

UPDATE KOCURY
SET PRZYDZIAL_MYSZY = 34
WHERE pseudo IN ('PUSZYSTA', 'LOLA', 'LASKA');

SELECT *
FROM KOCURY;

ROLLBACK;

SELECT *
FROM USER_TRIGGERS;

DROP TRIGGER wczytajPrzydzialTygrysa;
DROP TRIGGER utulZalMilusich;
DROP TRIGGER ukarzNagrodzTygrysa;