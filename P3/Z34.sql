SET SERVEROUTPUT ON;
DECLARE
    funkcja_kota KOCURY.FUNKCJA%TYPE := '&funkcja';
    liczba_kotow NUMBER;
BEGIN
    SELECT COUNT(pseudo)
    INTO liczba_kotow
    FROM KOCURY
    WHERE FUNKCJA = funkcja_kota;

    IF liczba_kotow > 0 THEN
        DBMS_OUTPUT.PUT_LINE('Znaleziono koty pelniace funkcje ' || funkcja_kota);
    ELSE
        DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota pelniacego funkcje ' || funkcja_kota);
    END IF;
END;