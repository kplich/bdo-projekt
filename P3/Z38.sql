DECLARE
    zadanaGlebokosc NUMBER(5) := &iloscSzefow;
    maxGlebokosc NUMBER(5);
    zapytanie VARCHAR2(10000) := '';
    wyswietlanie VARCHAR2(10000) := '';
    niedodatniaGlebokosc EXCEPTION;
BEGIN
    -- znajdz maksymalna glebokosc drzewa
    SELECT MAX(level) - 1
    INTO maxGlebokosc
    FROM KOCURY
    WHERE funkcja IN ('KOT', 'MILUSIA')
    CONNECT BY PRIOR pseudo = szef
    START WITH szef IS NULL;

    -- jesli zadana glebokosc jest niedodatnia lub wieksza od maksymalnej
    IF zadanaGlebokosc < 1 THEN
        RAISE niedodatniaGlebokosc;
    ELSIF zadanaGlebokosc > maxGlebokosc THEN
        zadanaGlebokosc := maxGlebokosc;   
    END IF;

    -- utworz klauzule select
    zapytanie := 'SELECT K1.imie "Imie", ';
    zapytanie := zapytanie || 'K1.funkcja "Funkcja", ';    

    -- i dolacz do niej koty z kolejnych joinow
    FOR i IN 1..zadanaGlebokosc - 1 LOOP
        zapytanie := zapytanie || 'NVL(K' || TO_CHAR(i + 1) || '.imie, '' '') "Szef ' || TO_CHAR(i) || '", ';
    END LOOP;
    zapytanie := zapytanie || 'NVL(K' || TO_CHAR(zadanaGlebokosc + 1) || '.imie, '' '') "Szef ' || TO_CHAR(zadanaGlebokosc) || '" ';

    -- nastepnie dolacz kolejne tabele
    zapytanie := zapytanie || 'FROM Kocury K1 JOIN Kocury K2 ON K1.szef = K2.pseudo AND K1.funkcja IN (''KOT'', ''MILUSIA'') ';
    FOR i IN 2..zadanaGlebokosc LOOP
        zapytanie := zapytanie || 'LEFT JOIN Kocury K' || TO_CHAR(i+1) || ' ON K' || TO_CHAR(i) || '.szef = K' || TO_CHAR(i+1) || '.pseudo '; 
    END LOOP;

    DBMS_OUTPUT.PUT_LINE(zapytanie);     

    -- polecenie do wyswietlania krotek
    wyswietlanie := 'RPAD(krotka."Imie", 10)';
    FOR i in 1..zadanaGlebokosc LOOP
        wyswietlanie := wyswietlanie || ' || '' | '' || RPAD(krotka."Szef ' || (i) || '", 10)';
    END LOOP;

    DBMS_OUTPUT.PUT_LINE(wyswietlanie);

    -- wyswietlanie naglowka
    DBMS_OUTPUT.PUT(RPAD('Imie', 10));
    FOR i IN 1..(zadanaGlebokosc) LOOP
        DBMS_OUTPUT.PUT(' | ' || RPAD('Szef ' || i, 10));
    END LOOP;
    DBMS_OUTPUT.PUT_LINE(' ');
    
    -- wyswietlanie dynamicznym kursorem
    EXECUTE IMMEDIATE 'DECLARE
                           CURSOR kursor IS ' || zapytanie || ';
                       BEGIN
                           FOR krotka IN kursor LOOP
                               DBMS_OUTPUT.PUT_LINE(' || wyswietlanie || ');
                           END LOOP;
                      END;'; 

EXCEPTION
    WHEN niedodatniaGlebokosc THEN
        DBMS_OUTPUT.PUT_LINE('Zadana glebokosc nie moze byc mniejsza lub rowna 0');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;