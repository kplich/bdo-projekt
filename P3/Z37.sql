BEGIN
    FOR kot IN (SELECT ROWNUM numer, pseudo, myszy_calk
                FROM (
                    SELECT pseudo, NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0) myszy_calk
                    FROM Kocury
                    ORDER BY myszy_calk DESC)
                WHERE ROWNUM <= 5) LOOP
        DBMS_OUTPUT.PUT_LINE(kot.numer || ' ' || kot.pseudo || ' ' || kot.myszy_calk);
    END LOOP;
END;