CREATE OR REPLACE TRIGGER sprawdzNumerBandy
BEFORE INSERT ON BANDY
FOR EACH ROW
DECLARE
    najwyzszyNumerBandy BANDY.NR_BANDY%TYPE;
BEGIN
    SELECT MAX(nr_bandy) INTO najwyzszyNumerBandy FROM Bandy;

    IF najwyzszyNumerBandy + 1 != :NEW.nr_bandy THEN
        RAISE_APPLICATION_ERROR(-20001, 'Numer nowej bandy musi byc wiekszy o 1 od najwyzszego numeru bandy!');
    END IF;
END;

BEGIN
    dodajBande(-7, 'SZEFOSTWO', 'SAD');
    dodajBande(7, 'SZEFOSTWO', 'SAD');
    dodajBande(7, 'MALE KOTKI', 'SAD');
    dodajBande(7, 'MALE KOTKI', 'DOM');
END;

