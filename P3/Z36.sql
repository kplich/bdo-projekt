--brakuje jednego podwyzszenia!
DECLARE
    CURSOR wszystkie_kocury IS
        SELECT *
        FROM Kocury
            JOIN Funkcje USING(funkcja)
        ORDER BY PRZYDZIAL_MYSZY ASC;
    liczba_zmian NUMBER(2) := 0;
    kocur wszystkie_kocury%ROWTYPE;
    suma_przydzialow NUMBER(4);
    nowy_przydzial NUMBER(3);
BEGIN

    --powtarzaj obiegi wielokrotnie, zgodnie z potrzebami
    <<obiegi>>
    LOOP
        FOR kocur IN wszystkie_kocury LOOP

            CASE
                WHEN ROUND(1.1 * kocur.przydzial_myszy) > kocur.max_myszy
                AND kocur.przydzial_myszy < kocur.max_myszy THEN
                    liczba_zmian := liczba_zmian + 1;
                    nowy_przydzial := kocur.max_myszy;

                    UPDATE KOCURY K
                    SET K.przydzial_myszy = nowy_przydzial
                    WHERE K.pseudo = kocur.pseudo;
                WHEN ROUND(1.1 * kocur.przydzial_myszy) < kocur.max_myszy THEN
                    liczba_zmian := liczba_zmian + 1;
                    nowy_przydzial := ROUND(1.1 * kocur.przydzial_myszy);

                    UPDATE KOCURY K
                    SET K.przydzial_myszy = nowy_przydzial
                    WHERE K.pseudo = kocur.pseudo;
                ELSE
                    NULL;
            END CASE;

            --oblicz sume przydzialow
            SELECT SUM(NVL(przydzial_myszy, 0))
            INTO suma_przydzialow
            FROM Kocury;

            EXIT obiegi WHEN suma_przydzialow > 1050;
                     
        END LOOP;
    END LOOP obiegi;

    DBMS_OUTPUT.PUT_LINE('Calkowity przydzial w stadku: ' || suma_przydzialow);
    
    DBMS_OUTPUT.PUT_LINE('Dokonano ' || liczba_zmian || ' zmian.');

    FOR k IN (SELECT imie, NVL(przydzial_myszy, 0) pm FROM Kocury ORDER BY pm DESC) LOOP
        DBMS_OUTPUT.PUT_LINE(k.imie || ' ' || k.pm);
    END LOOP;

    ROLLBACK;
END;