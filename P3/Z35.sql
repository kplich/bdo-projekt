SET SERVEROUTPUT ON;
DECLARE
    pseudo_podane Kocury.pseudo%TYPE := '&pseudo';
    imie_znalezione Kocury.imie%TYPE;
    calk_przydzial Kocury.przydzial_myszy%TYPE;
    data_przystapienia Kocury.w_stadku_od%TYPE;
BEGIN
    SELECT imie, (NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) * 12, w_stadku_od
    INTO imie_znalezione, calk_przydzial, data_przystapienia
    FROM Kocury
    WHERE pseudo = pseudo_podane;

    DBMS_OUTPUT.PUT_LINE(imie_znalezione || ' ' || calk_przydzial || ' ' || data_przystapienia);    

    IF calk_przydzial > 700 THEN
        DBMS_OUTPUT.PUT_LINE(imie_znalezione || ' otrzymuje rocznie ponad 700 myszy');
    ELSIF (imie_znalezione LIKE '%A%') THEN
        DBMS_OUTPUT.PUT_LINE(imie_znalezione || ' zawiera w imieniu litere A');
    ELSIF EXTRACT(MONTH from data_przystapienia) = 1 THEN
        DBMS_OUTPUT.PUT_LINE(imie_znalezione || ' przystapil(a) do stadka w styczniu');
    ELSE
        DBMS_OUTPUT.PUT_LINE(imie_znalezione || ' nie odpowiada kryteriom');
    END IF;

EXCEPTION
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota o podanym pseudonimie.');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;