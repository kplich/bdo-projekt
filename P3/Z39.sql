DECLARE
    numerNowejBandy Bandy.NR_BANDY%TYPE := &numer;
    nazwaNowejBandy BANDY.NAZWA%TYPE := 'nazwa';
    terenNowejBandy BANDY.TEREN%TYPE := '&teren';

    liczbaWynikow NUMBER(5);

    numerBandyUjemny EXCEPTION;
    numerBandyIstnieje EXCEPTION;
    nazwaBandyIstnieje EXCEPTION;
    terenBandyIstnieje EXCEPTION;
BEGIN
    IF numerNowejBandy < 0 THEN
        RAISE numerBandyUjemny;
    END IF;

    SELECT COUNT(*) INTO liczbaWynikow FROM BANDY WHERE NR_BANDY = numerNowejBandy;
    IF 0 < liczbaWynikow THEN
        RAISE numerBandyIstnieje;
    END IF;

    SELECT COUNT(*) INTO liczbaWynikow FROM BANDY WHERE nazwa = nazwaNowejBandy;
    IF 0 < liczbaWynikow THEN
        RAISE nazwaBandyIstnieje;
    END IF;

    SELECT COUNT(*) INTO liczbaWynikow FROM BANDY WHERE teren = terenNowejBandy;
    IF 0 < liczbaWynikow THEN
        RAISE terenBandyIstnieje;
    END IF;

    INSERT INTO BANDY
    VALUES(numerNowejBandy, nazwaNowejBandy, terenNowejBandy, NULL);

    ROLLBACK;
    
EXCEPTION
    WHEN numerBandyUjemny THEN
        DBMS_OUTPUT.PUT_LINE('Numer bandy nie może być ujemny');
    WHEN numerBandyIstnieje THEN
        DBMS_OUTPUT.PUT_LINE('Banda o numerze ' || numerNowejBandy || ' już istnieje');
    WHEN nazwaBandyIstnieje THEN
        DBMS_OUTPUT.PUT_LINE('Banda o nazwie ' || nazwaNowejBandy || ' już istnieje');
    WHEN terenBandyIstnieje THEN
        DBMS_OUTPUT.PUT_LINE('Banda o terenie ' || terenNowejBandy || ' już istnieje');    
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;