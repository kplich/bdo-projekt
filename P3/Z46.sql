--zad46
CREATE TABLE Przekroczenia_Przedzialu
(
    kto      VARCHAR2(100),
    kiedy    DATE DEFAULT SYSDATE,
    komu     VARCHAR2(15),
    operacja VARCHAR2(100)
);

CREATE OR REPLACE TRIGGER przekroczenie_przedzialu
    BEFORE INSERT OR UPDATE
    ON KOCURY
    FOR EACH ROW
DECLARE
    min_myszy FUNKCJE.MIN_MYSZY%TYPE;
    max_myszy FUNKCJE.MAX_MYSZY%TYPE;
    operacja  VARCHAR2(30);
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT FUNKCJE.MIN_MYSZY, FUNKCJE.MAX_MYSZY
    INTO min_myszy, max_myszy
    FROM FUNKCJE
    WHERE FUNKCJE.FUNKCJA = :NEW.FUNKCJA;

    IF INSERTING THEN
        operacja := 'INSERT';
    ELSIF UPDATING THEN
        operacja := 'UPDATE';
    end if;

    IF :NEW.PRZYDZIAL_MYSZY < min_myszy OR :NEW.PRZYDZIAL_MYSZY > max_myszy THEN
        :NEW.PRZYDZIAL_MYSZY := :OLD.PRZYDZIAL_MYSZY;
        INSERT INTO Przekroczenia_Przedzialu(kto, komu, operacja) VALUES (ORA_LOGIN_USER(), :NEW.pseudo, operacja);
        COMMIT;
    end if;
END;

UPDATE KOCURY
SET PRZYDZIAL_MYSZY = 900
WHERE PSEUDO = 'LOLA';

ROLLBACK;

SELECT *
FROM Przekroczenia_Przedzialu;

DROP TABLE Przekroczenia_Przedzialu;
DROP TRIGGER przekroczenie_przedzialu;