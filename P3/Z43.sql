DECLARE
    zapytanie VARCHAR2(10000) := 'SELECT  DECODE(plec, ''D'', nazwa, NULL, ''ZJADA RAZEM'', '' '') "Nazwa bandy",
        DECODE(plec, ''M'', ''Kocur'', ''D'', ''Kotka'') "Plec",
        ILE,
        <kolumnySelect>
FROM (
    SELECT *
    FROM (
        SELECT *
        FROM (
            SELECT nazwa, plec, NVL(funkcja, ''SUMA'') fun, SUM(przydzial_myszy + NVL(myszy_extra, 0)) myszy_calk
            FROM Kocury
                NATURAL JOIN Bandy
            GROUP BY GROUPING SETS(
                -- suma przydzialow w bandzie dla kazdej funkcji ze wzgledu na plec
                (nazwa, plec, funkcja),

                -- suma przydzialow w bandzie ze wzgledu na plec (pseudo funkcja ''SUMA'')
                (nazwa, plec),

                -- suma przydzialow w funkcji
                (funkcja),

                -- suma przydzialow w stadku
                ()
            )
        )
        WHERE plec IS NOT NULL AND nazwa IS NOT NULL OR (plec IS NULL AND nazwa IS NULL)
    )
    PIVOT (
        SUM(myszy_calk)
        FOR fun
        IN (<wartosciPivot>)
    )
)
    LEFT JOIN (
        SELECT nazwa, plec, COUNT(pseudo) ILE
        FROM Kocury
            NATURAL JOIN Bandy
        GROUP BY nazwa, plec)
    USING(nazwa, plec)
ORDER BY nazwa, plec';
    kolumnySelect VARCHAR2(10000);
    wartosciPivot VARCHAR2(10000);
    naglowek VARCHAR2(10000) := '    Nazwa bandy |  Plec | ILE | ';
    wyswietlanie VARCHAR2(10000) := 'LPAD(krotka."Nazwa bandy", 15) || '' | '' || LPAD(NVL(krotka."Plec", '' ''), 5) || '' | '' || LPAD(NVL(TO_CHAR(krotka.ILE), '' ''), 3) || '' | '' || ';
BEGIN
    -- znajdz wszystkie funkcje
    FOR krotkaFunkcji IN (SELECT FUNKCJA
                    FROM KOCURY
                    GROUP BY FUNKCJA) LOOP
        kolumnySelect := kolumnySelect || 'NVL(' || krotkaFunkcji.FUNKCJA || ', 0) ' || krotkaFunkcji.FUNKCJA ||', ';
        wartosciPivot := wartosciPivot || '''' || krotkaFunkcji.FUNKCJA || ''' ' || krotkaFunkcji.FUNKCJA || ', ';
        naglowek := naglowek || LPAD(krotkaFunkcji.FUNKCJA, 15) || ' | ';
        wyswietlanie := wyswietlanie || 'LPAD(krotka.' || krotkaFunkcji.FUNKCJA || ', 15) || '' | '' || ';
    end loop;

    kolumnySelect := kolumnySelect || 'SUMA';
    wartosciPivot := wartosciPivot || '''SUMA'' SUMA';
    wyswietlanie := wyswietlanie || 'LPAD(krotka.SUMA, 4)';
    naglowek := naglowek || 'SUMA';

    zapytanie := REPLACE(zapytanie, '<kolumnySelect>', kolumnySelect);
    zapytanie := REPLACE(zapytanie, '<wartosciPivot>', wartosciPivot);

    DBMS_OUTPUT.PUT_LINE(naglowek);
    EXECUTE IMMEDIATE 'DECLARE
                           CURSOR kursor IS ' || zapytanie || ';
                       BEGIN
                           FOR krotka IN kursor LOOP
                               DBMS_OUTPUT.PUT_LINE(' || wyswietlanie || ');
                           END LOOP;
                       END;';
end;