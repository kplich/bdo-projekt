--zad 42
CREATE OR REPLACE TRIGGER milusiowyTrigger
FOR UPDATE OF PRZYDZIAL_MYSZY ON KOCURY
WHEN (OLD.FUNKCJA = 'MILUSIA' AND NEW.FUNKCJA = 'MILUSIA')
COMPOUND TRIGGER
    przydzialMyszyTygrysa NUMBER(5) := 0;
    karaDlaTygrysa NUMBER(5) := 0;
    nagrodaDlaTygrysa NUMBER(5) := 0;

    BEFORE STATEMENT IS
    BEGIN
        SELECT NVL(PRZYDZIAL_MYSZY, 0)
        INTO przydzialMyszyTygrysa
        FROM KOCURY
        WHERE PSEUDO = 'TYGRYS';

    END BEFORE STATEMENT;

    BEFORE EACH ROW IS
        roznicaWPrzydziale NUMBER(3) := 0;
    BEGIN
        DBMS_OUTPUT.PUT_LINE('utulam zal milusi');
        roznicaWPrzydziale := :NEW.przydzial_myszy - :OLD.przydzial_myszy;

        -- jesli nastepuje proba zmniejszenia przydzialu, zablokuj ja
        IF (roznicaWPrzydziale < 0) THEN
            :NEW.przydzial_myszy := :OLD.przydzial_myszy;
        end if;

        IF (roznicaWPrzydziale > 0) THEN
            IF (roznicaWPrzydziale < ROUND(0.1 * przydzialMyszyTygrysa)) THEN
                DBMS_OUTPUT.PUT_LINE('before changes');
                DBMS_OUTPUT.PUT_LINE('old: ' || :old.IMIE || ', pm: ' || :old.PRZYDZIAL_MYSZY || ', me: ' || :old.MYSZY_EXTRA);
                DBMS_OUTPUT.PUT_LINE('new: ' || :NEW.IMIE || ', pm: ' || :new.PRZYDZIAL_MYSZY || ', me: ' || :new.MYSZY_EXTRA);
                :NEW.przydzial_myszy := :OLD.przydzial_myszy + ROUND(0.1 * przydzialMyszyTygrysa);
                :NEW.myszy_extra := :NEW.myszy_extra + 5;

                DBMS_OUTPUT.PUT_LINE('after changes');
                DBMS_OUTPUT.PUT_LINE('old: ' || :old.IMIE || ', pm: ' || :old.PRZYDZIAL_MYSZY || ', me: ' || :old.MYSZY_EXTRA);
                DBMS_OUTPUT.PUT_LINE('new: ' || :NEW.IMIE || ', pm: ' || :new.PRZYDZIAL_MYSZY || ', me: ' || :new.MYSZY_EXTRA);

                karaDlaTygrysa := karaDlaTygrysa + 0.1 * przydzialMyszyTygrysa;
            ELSE
                nagrodaDlaTygrysa := nagrodaDlaTygrysa + 5;
            END IF;
        end if;
        DBMS_OUTPUT.PUT_LINE('skonczylem utulac zal milusi');
    end BEFORE EACH ROW;

    AFTER STATEMENT IS
    BEGIN
        DBMS_OUTPUT.PUT_LINE('begin trigger nagroda tygrysa');
        DBMS_OUTPUT.PUT_LINE('kara: ' || karaDlaTygrysa);
        DBMS_OUTPUT.PUT_LINE('nagroda: ' || nagrodaDlaTygrysa);

        IF (karaDlaTygrysa > 0 AND nagrodaDlaTygrysa > 0) THEN

            DBMS_OUTPUT.PUT_LINE('updating tygrys');

            UPDATE KOCURY
            SET PRZYDZIAL_MYSZY = PRZYDZIAL_MYSZY - karaDlaTygrysa, MYSZY_EXTRA = MYSZY_EXTRA + nagrodaDlaTygrysa
            WHERE PSEUDO = 'TYGRYS';

            DBMS_OUTPUT.PUT_LINE('updated tygrys');
        END IF;

        DBMS_OUTPUT.PUT_LINE('end trigger nagroda tygrysa');
    END AFTER STATEMENT;
END milusiowyTrigger;

SELECT * FROM KOCURY;

UPDATE KOCURY
SET PRZYDZIAL_MYSZY = 34
WHERE pseudo IN ('PUSZYSTA', 'LOLA', 'LASKA');

SELECT * FROM KOCURY;

ROLLBACK;

DROP TRIGGER milusiowyTrigger;
