--zad 45
CREATE TABLE Dodatki_extra
(
    id_dodatku    NUMBER(2) GENERATED BY DEFAULT ON NULL AS IDENTITY
        CONSTRAINT dx_pk PRIMARY KEY,
    pseudo        VARCHAR2(15)
        CONSTRAINT dx_fk_ko REFERENCES Kocury (pseudo),
    dodatek_extra NUMBER(5) NOT NULL
);

CREATE OR REPLACE TRIGGER kara_dla_milus
    BEFORE UPDATE OF PRZYDZIAL_MYSZY
    ON KOCURY
    FOR EACH ROW
    WHEN (OLD.funkcja = 'MILUSIA')
BEGIN
    IF :OLD.przydzial_myszy < :NEW.przydzial_myszy THEN
        :NEW.przydzial_myszy := :OLD.przydzial_myszy;
        DBMS_OUTPUT.put_line('Uzytkownik bez uprawnien');
        INSERT INTO Dodatki_extra(pseudo, dodatek_extra)
        VALUES (:OLD.PSEUDO, -10);
    END IF;
END;

SELECT *
FROM KOCURY;


UPDATE KOCURY
SET przydzial_myszy = 28
WHERE KOCURY.PSEUDO = 'LOLA';

ROLLBACK;
SELECT *
FROM DODATKI_EXTRA;

SELECT *
FROM USER_TRIGGERS;

DROP TABLE Dodatki_extra;
DROP TRIGGER kara_dla_milus;

