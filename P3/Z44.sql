CREATE OR REPLACE PACKAGE poglowne AS
    FUNCTION podatek(pseudonim VARCHAR2) RETURN NUMBER;
    PROCEDURE wstawBande(nowyNumer NUMBER, nowaNazwa VARCHAR2, nowyTeren VARCHAR2);
END;

CREATE OR REPLACE PACKAGE BODY poglowne AS

    PROCEDURE wstawBande(nowyNumer NUMBER, nowaNazwa VARCHAR2, nowyTeren VARCHAR2)
    AS
        NIEPOPRAWNY_NUMER EXCEPTION;
        NIEPOPRAWNE_WARTOSCI EXCEPTION;
        blad          BOOLEAN    := FALSE;
        czyZnaleziono NUMBER     := 0;
        wiadomosc     STRING(60) := '';
    BEGIN
        IF nowyNumer <= 0 THEN
            RAISE NIEPOPRAWNY_NUMER;
        END IF;

        SELECT COUNT(*)
        INTO czyZnaleziono
        FROM BANDY
        WHERE NR_BANDY = nowyNumer;

        IF czyZnaleziono > 0
        THEN
            wiadomosc := wiadomosc || ' ' || nowyNumer;
            blad := TRUE;
        END IF;

        SELECT COUNT(*)
        INTO czyZnaleziono
        FROM BANDY
        WHERE NAZWA = nowaNazwa;

        IF czyZnaleziono > 0
        THEN
            IF INSTR(wiadomosc, ' ') != 0 THEN
                wiadomosc := wiadomosc || ', ';
            ELSE
                wiadomosc := wiadomosc || ' ';
            END IF;
            wiadomosc := wiadomosc || nowaNazwa;
            blad := TRUE;
        END IF;

        SELECT COUNT(*)
        INTO czyZnaleziono
        FROM BANDY
        WHERE TEREN = nowyTeren;

        IF czyZnaleziono > 0
        THEN
            IF INSTR(wiadomosc, ' ') != 0 THEN
                wiadomosc := wiadomosc || ', ';
            ELSE
                wiadomosc := wiadomosc || ' ';
            END IF;
            wiadomosc := wiadomosc || nowyTeren;
            blad := TRUE;
        END IF;

        IF blad THEN
            RAISE NIEPOPRAWNE_WARTOSCI;
        END IF;

        INSERT INTO BANDY (nr_bandy, nazwa, teren) VALUES (nowyNumer, nowaNazwa, nowyTeren);
        DBMS_OUTPUT.put_line('WSTAWIONO BANDE');

    EXCEPTION
        WHEN NIEPOPRAWNY_NUMER THEN DBMS_OUTPUT.put_line('WPISANO NIEPOPRAWNY NUMER');
        WHEN NIEPOPRAWNE_WARTOSCI THEN DBMS_OUTPUT.put_line(wiadomosc || ': juz istnieje');
        WHEN OTHERS THEN DBMS_OUTPUT.put_line(SQLERRM);
    END;

    FUNCTION podatek(pseudonim VARCHAR2) RETURN NUMBER AS
        doOddania NUMBER(5) := 0;
        podwladni NUMBER(5) := 0;
        wrogowie NUMBER(5) := 0;
        podatekOdBogactwa NUMBER(5) := 0;
    BEGIN
        SELECT CEIL((NVL(przydzial_myszy, 0) + NVL(MYSZY_EXTRA, 0)) * 0.05)
        INTO doOddania
        FROM Kocury
        WHERE pseudo = pseudonim;

        SELECT COUNT(*)
        INTO podwladni
        FROM Kocury
        WHERE szef = pseudonim;

        IF (podwladni = 0) THEN
            doOddania := doOddania + 2;
        end if;

        SELECT COUNT(*)
        INTO wrogowie
        FROM Kocury JOIN Wrogowie_Kocurow ON KOCURY.PSEUDO = WROGOWIE_KOCUROW.PSEUDO
        WHERE Kocury.pseudo = pseudonim;

        IF (wrogowie = 0) THEN
            doOddania := doOddania + 1;
        end if;

        SELECT CEIL(NVL(myszy_extra, 0) * 0.1)
        INTO podatekOdBogactwa
        FROM Kocury
        WHERE pseudo = pseudonim;

        doOddania := doOddania + podatekOdBogactwa;

        RETURN doOddania;
    END;
END;

SELECT pseudo "PSEUDO", poglowne.podatek(pseudo) "PODATEK"
FROM kocury;
