ALTER SESSION set nls_date_format='YYYY-MM-DD';

-- LISTA 1 ---------------------------------------------------------------------------------------------------------------------

-- zadanie 1
SELECT DISTINCT imie_wroga "Imie wroga", opis_incydentu "Przewina"
FROM Wrogowie_kocurow
WHERE EXTRACT(YEAR FROM data_incydentu) = '2009';

-- zadanie 2
SELECT imie, funkcja, w_stadku_od "Z nami od"
FROM Kocury
WHERE plec='D' AND w_stadku_od BETWEEN '2005-09-01' AND '2007-07-31';

-- zadanie 3
SELECT imie_wroga "Wrog", gatunek, stopien_wrogosci
FROM Wrogowie
WHERE lapowka IS NULL
ORDER BY stopien_wrogosci;

-- zadanie 4
SELECT imie || ' zwany ' || pseudo || ' (fun. ' || funkcja || ') lowi myszki w bandzie ' || nr_bandy || ' od ' || w_stadku_od "Wszystko o kocurach"
FROM Kocury
WHERE plec='M'
ORDER BY w_stadku_od DESC, pseudo ASC;

-- zadanie 5
SELECT pseudo, REGEXP_REPLACE(REGEXP_REPLACE(pseudo, 'A', '#', 1, 1), 'L', '%', 1, 1) "Po wymianie A na # oraz L na %"
FROM Kocury
WHERE pseudo LIKE '%A%L%' OR pseudo LIKE '%L%A%';

-- zadanie 6
SELECT imie,
       w_stadku_od "W stadku",
       ROUND(przydzial_myszy / 1.1) "Zjadal",
       ADD_MONTHS(w_stadku_od, 6) "Podwyzka",
       przydzial_myszy "Zjada"
FROM Kocury
WHERE MONTHS_BETWEEN(SYSDATE, w_stadku_od) >= 120
      AND EXTRACT(MONTH FROM w_stadku_od) BETWEEN 3 AND 9;
      
-- zadanie 7
SELECT imie, 3 * NVL(przydzial_myszy, 0) "Myszy kwartalnie", 3 * NVL(myszy_extra, 0) "Kwartalne dodatki"
FROM Kocury
WHERE NVL(przydzial_myszy, 0) > 2 * NVL(myszy_extra, 0) AND NVL(przydzial_myszy, 0) >= 55
ORDER BY "Myszy kwartalnie" DESC;

-- zadanie 8
SELECT imie, CASE WHEN (NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) * 12 < 660 THEN 'Ponizej 660'
                  WHEN (NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) * 12 = 660 THEN 'Limit'
                  ELSE TO_CHAR((NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) * 12)
             END "Zjada rocznie"
FROM Kocury;

-- zadanie 9
SELECT pseudo, w_stadku_od, CASE WHEN EXTRACT(DAY FROM w_stadku_od) BETWEEN 1 AND 15 THEN
                                 CASE WHEN NEXT_DAY(LAST_DAY('2019-09-24') - 7, '�RODA') >= '2019-09-24' THEN NEXT_DAY(LAST_DAY('2019-09-24') - 7, '�RODA')
                                      ELSE NEXT_DAY(LAST_DAY(ADD_MONTHS('2019-09-24', 1)) - 7, '�RODA')
                                      END
                                 ELSE NEXT_DAY(LAST_DAY(ADD_MONTHS('2019-09-24', 1)) - 7, '�RODA')
                                 END "Wyplata"   
FROM Kocury;

-- zadanie 10
SELECT pseudo || ' - ' || DECODE(Count(szef), 1, 'Unikalny', 'Nieunikalny') "Unikalnosc atr. PSEUDO"
FROM Kocury
GROUP BY pseudo;

SELECT szef || ' - ' || DECODE(Count(szef), 1, 'Unikalny', 'Nieunikalny') "Unikalnosc atr. szef"
FROM Kocury
GROUP BY szef
    HAVING szef IS NOT NULL;
    
-- zadanie 11
SELECT pseudo, COUNT(imie_wroga)
FROM Wrogowie_kocurow
GROUP BY pseudo
    HAVING COUNT(imie_wroga) >= 2;

-- zadanie 12
SELECT 'Liczba kot�w= ' " ",
       COUNT(pseudo) " ",
       '�owi jako' " ",
       funkcja " ",
       'i zjada max' " ",
       MAX(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) " ",
       'myszy miesiecznie' " "
FROM Kocury
WHERE plec != 'M'
GROUP BY funkcja
    HAVING funkcja != 'SZEFUNIO' AND AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) > 50;
    
-- zadanie 13
SELECT nr_bandy, plec, MIN(NVL(przydzial_myszy, 0))
FROM Kocury
GROUP BY nr_bandy, plec;

-- zadanie 14
SELECT level "Poziom", pseudo "Pseudonim", funkcja, nr_bandy "Nr bandy"
FROM Kocury
WHERE plec = 'M'
CONNECT BY PRIOR pseudo = szef
START WITH funkcja = 'BANDZIOR' AND plec = 'M';

-- zadanie 15
SELECT RPAD('===>', (level - 1) * 4, '===>') || (level - 1) || '          ' || imie "Hierarchia", NVL(szef, 'Sam sobie panem') "Pseudo szefa", funkcja
FROM Kocury
WHERE myszy_extra IS NOT NULL
CONNECT BY PRIOR pseudo = szef
START WITH szef IS NULL;

-- zadanie 16
SELECT RPAD(' ', (level - 1) * 4, '    ') || pseudo
FROM Kocury
START WITH (myszy_extra IS NULL OR myszy_extra = 0) AND MONTHS_BETWEEN('2019-07-03', w_stadku_od) >= 120 AND plec = 'M'
CONNECT BY PRIOR szef = pseudo;

-- LISTA 2 ---------------------------------------------------------------------------------------------------------------------

-- zadanie 17
SELECT pseudo "Poluje w polu", przydzial_myszy, nazwa
FROM Kocury JOIN Bandy 
              ON Kocury.nr_bandy = Bandy.nr_bandy AND teren IN ('POLE', 'CALOSC') AND przydzial_myszy > 50
ORDER BY przydzial_myszy DESC;

-- zadanie 18
SELECT K1.imie, K1.w_stadku_od
FROM Kocury K1 JOIN Kocury K2
                 ON K1.w_stadku_od < K2.w_stadku_od AND K2.imie = 'JACEK'
ORDER BY K1.w_stadku_od DESC;

-- zadanie 19
-- a
SELECT K1.imie "Imie", K1.funkcja "Funkcja", NVL(K2.imie,' ') "Szef 1", NVL(K3.imie,' ') "Szef 2", NVL(K4.imie,' ') "Szef 3"
FROM Kocury K1
        JOIN Kocury K2 ON K1.szef = K2.pseudo AND K1.funkcja IN ('KOT', 'MILUSIA')
        LEFT JOIN Kocury K3 ON K2.szef = K3.pseudo
        LEFT JOIN Kocury K4 ON K3.szef = K4.pseudo;
        
-- b
SELECT "Imie", ' | ' " ", "Funkcja", ' | ' "  ", NVL(Szef1, ' ') "Szef 1", ' | ' "   ", NVL(Szef2, ' ') "Szef 2", ' | ' "    ", NVL(Szef3, ' ') "Szef 3"
FROM (SELECT CONNECT_BY_ROOT NVL(imie,'  ') "Imie", CONNECT_BY_ROOT funkcja "Funkcja", NVL(imie,'  ') "Szef", level poziom
        FROM Kocury
        CONNECT BY PRIOR szef = pseudo
        START WITH funkcja IN ('KOT', 'MILUSIA'))
PIVOT (MAX("Szef")
        FOR poziom
        IN(2 Szef1, 3 Szef2, 4 Szef3));
            
-- c
SELECT CONNECT_BY_ROOT imie "Imie", ' | ' " ", CONNECT_BY_ROOT funkcja "Funkcja", SYS_CONNECT_BY_PATH(NVL(imie,' '), ' | ') "Imiona kolejnych szefów"
FROM Kocury 
WHERE CONNECT_BY_ISLEAF = 1
CONNECT BY PRIOR szef = pseudo 
START WITH funkcja IN ('KOT', 'MILUSIA');

-- zadanie 20
SELECT imie, nazwa, imie_wroga, stopien_wrogosci, data_incydentu
FROM Kocury
    JOIN Bandy ON Kocury.nr_bandy = Bandy.nr_bandy AND plec = 'D'
    JOIN Wrogowie_kocurow ON Kocury.pseudo = Wrogowie_kocurow.pseudo AND data_incydentu > '2007-01-01'
    JOIN Wrogowie USING(imie_wroga);
            
-- zadanie 21
SELECT nazwa, COUNT(DISTINCT pseudo)
FROM Bandy
    JOIN Kocury USING(nr_bandy)
    JOIN Wrogowie_kocurow USING(pseudo)        
GROUP BY nazwa;

-- zadanie 22
SELECT pseudo, funkcja, COUNT(imie_wroga)
FROM Kocury JOIN Wrogowie_kocurow USING(pseudo)
GROUP BY pseudo, funkcja
    HAVING COUNT(imie_wroga) > 1;
    
-- zadanie 23
SELECT imie, (przydzial_myszy + myszy_extra) * 12 "DAWKA ROCZNA", 'powyzej 864' "DAWKA"
FROM Kocury
WHERE myszy_extra IS NOT NULL AND (przydzial_myszy + myszy_extra) * 12 > 864

UNION

SELECT imie, (przydzial_myszy + myszy_extra) * 12 "DAWKA ROCZNA", '864' "DAWKA"
FROM Kocury
WHERE myszy_extra IS NOT NULL AND (przydzial_myszy + myszy_extra) * 12 = 864

UNION

SELECT imie, (przydzial_myszy + myszy_extra) * 12 "DAWKA ROCZNA", 'ponizej 864' "DAWKA"
FROM Kocury
WHERE myszy_extra IS NOT NULL AND (przydzial_myszy + myszy_extra) * 12 < 864

ORDER BY "DAWKA ROCZNA" DESC;

-- zadanie 24
-- bez podzapytan
SELECT nr_bandy, nazwa, teren
FROM Bandy LEFT JOIN Kocury USING(nr_bandy)
GROUP BY nr_bandy, nazwa, teren
    HAVING COUNT(pseudo) = 0;

-- wykorzystujac operatory zbiorowe
-- wszystkie bandy
SELECT nr_bandy, nazwa, teren
FROM Bandy LEFT JOIN Kocury USING(nr_bandy)

MINUS

-- bandy z kotami
SELECT nr_bandy, nazwa, teren
FROM Bandy JOIN Kocury USING(nr_bandy)

GROUP BY nr_bandy, nazwa, teren;
    
-- zadanie 25
SELECT imie, funkcja, przydzial_myszy "pm"
FROM Kocury 
WHERE przydzial_myszy >= ALL(SELECT 3 * przydzial_myszy
                             FROM Kocury NATURAL JOIN Bandy
                             WHERE funkcja = 'MILUSIA' AND teren IN ('SAD', 'CALOSC'));
                            
-- zadanie 26
SELECT funkcja, ROUND(AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0))) "najw. i najmn. przydzial"
FROM Kocury
GROUP BY funkcja
    HAVING funkcja != 'SZEFUNIO'
    AND (AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) >= ALL(SELECT AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0))
                                                                   FROM Kocury
                                                                   GROUP BY funkcja
                                                                       HAVING funkcja != 'SZEFUNIO')
    OR AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) <= ALL(SELECT AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0))
                                                                 FROM Kocury
                                                                 GROUP BY funkcja
                                                                    HAVING funkcja != 'SZEFUNIO'));
                                                               
-- zadanie 27
-- a
-- dla kazdego kota liczymy ile jest przydzialow wiekszych od niego; dopoki jest to mniej niz n, to wybieramy go;
SELECT K1.pseudo, (K1.przydzial_myszy + NVL(K1.myszy_extra, 0)) "Zjada"
FROM Kocury K1
WHERE (
    SELECT COUNT(DISTINCT przydzial_myszy)
    FROM Kocury K2
    WHERE (K1.przydzial_myszy + NVL(K1.myszy_extra, 0) < K2.przydzial_myszy + NVL(K2.myszy_extra, 0))
) < &n
ORDER BY "Zjada" DESC;

--b
-- najpierw sortujemy  przydzialy malejaco, potem wybieramy 6 najwiekszych i wybieramy koty, ktore je maja
SELECT pseudo, przydzial_myszy + NVL(myszy_extra, 0)
FROM Kocury
WHERE przydzial_myszy + NVL(myszy_extra, 0) IN (SELECT "Zjada2"
                                                FROM (SELECT DISTINCT przydzial_myszy + NVL(myszy_extra, 0) "Zjada2"
                                                      FROM Kocury
                                                      ORDER BY "Zjada2" DESC)
                                                WHERE ROWNUM <= &n);
                                                
--c
-- kazdy kot jest polaczony z kotami, ktore jedza wiecej lub tyle samo co on,
-- wybieramy te koty, ktore maja mniej lub rowno n wiekszych przydzialow
SELECT K1.pseudo, K1.przydzial_myszy + NVL(K1.myszy_extra, 0) "Zjada"
FROM Kocury K1
    JOIN Kocury K2 ON (K1.przydzial_myszy + NVL(K1.myszy_extra, 0) <= K2.przydzial_myszy + NVL(K2.myszy_extra, 0))
GROUP BY K1.pseudo, K1.przydzial_myszy + NVL(K1.myszy_extra, 0)
    HAVING COUNT(DISTINCT K2.przydzial_myszy + NVL(K2.myszy_extra, 0)) <= &n
ORDER BY "Zjada" DESC;

--d
--korzystamy z funkcji dense rank aby wyznaczyc pozycje kazdego kota w 'rankingu przydzialow'
SELECT pseudo, "ZJADA"
FROM (SELECT pseudo, przydzial_myszy + NVL(myszy_extra, 0) "ZJADA",
        DENSE_RANK()
        OVER (ORDER BY przydzial_myszy + NVL(myszy_extra, 0) DESC) pozycja
      FROM Kocury)
WHERE pozycja <= &n;

-- zadanie 28
WITH
    "mniejsze_od_sredniej" AS (
        SELECT TO_CHAR(EXTRACT(YEAR FROM w_stadku_od)) AS "ROK", COUNT(pseudo) AS "LICZBA WSTAPIEN"
        FROM Kocury
        GROUP BY EXTRACT(YEAR FROM w_stadku_od)
            HAVING COUNT(pseudo) < (SELECT AVG(COUNT(pseudo))
                                    FROM Kocury
                                    GROUP BY EXTRACT(YEAR FROM w_stadku_od))),
    "wieksze_od_sredniej" AS (
        SELECT TO_CHAR(EXTRACT(YEAR FROM w_stadku_od)) AS "ROK", COUNT(pseudo) AS "LICZBA WSTAPIEN"
        FROM Kocury
        GROUP BY EXTRACT(YEAR FROM w_stadku_od)
            HAVING COUNT(pseudo) > (SELECT AVG(COUNT(pseudo))
                                    FROM Kocury
                                    GROUP BY EXTRACT(YEAR FROM w_stadku_od)))
SELECT *
FROM "mniejsze_od_sredniej"
WHERE "LICZBA WSTAPIEN" = (SELECT MAX("LICZBA WSTAPIEN")
                           FROM "mniejsze_od_sredniej")

UNION

SELECT 'Srednia' "ROK", ROUND(AVG(COUNT(pseudo)), 2) "LICZBA WSTAPIEN"
FROM Kocury
GROUP BY EXTRACT(YEAR FROM w_stadku_od)

UNION

SELECT *
FROM "wieksze_od_sredniej"
WHERE "LICZBA WSTAPIEN" = (SELECT MIN("LICZBA WSTAPIEN")
                           FROM "wieksze_od_sredniej")

ORDER BY "LICZBA WSTAPIEN" ASC;

-- zadanie 29
-- a
-- laczymy kazdego kocura z jego kolegami z bandy
SELECT K1.imie, K1.nr_bandy, K1.przydzial_myszy + NVL(K1.myszy_extra, 0), TO_CHAR(ROUND(AVG(K2.przydzial_myszy + NVL(K2.myszy_extra, 0)), 2), '99.99')
FROM Kocury K1
    JOIN Kocury K2 ON K1.nr_bandy = K2.nr_bandy AND K1.plec = 'M'
GROUP BY K1.imie, K1.nr_bandy, K1.przydzial_myszy + NVL(K1.myszy_extra, 0)
    HAVING K1.przydzial_myszy + NVL(K1.myszy_extra, 0) < AVG(K2.przydzial_myszy + NVL(K2.myszy_extra, 0));

-- b
-- laczymy kazdego kocura ze srednia jego bandy
SELECT K1.imie, K1.nr_bandy, K1.przydzial_myszy + NVL(K1.myszy_extra, 0), TO_CHAR(ROUND("srednia w bandzie", 2), '99.99')
FROM Kocury K1
    JOIN (SELECT nr_bandy, AVG(przydzial_myszy + NVL(myszy_extra, 0)) "srednia w bandzie"
            FROM Kocury
            GROUP BY nr_bandy) K2
    ON K1.nr_bandy = K2.nr_bandy AND K1.plec = 'M' AND K1.przydzial_myszy + NVL(K1.myszy_extra, 0) < "srednia w bandzie";

--c
-- dla kazdego kocura obliczamy srednia w jego bandzie
SELECT imie, przydzial_myszy + NVL(myszy_extra, 0), TO_CHAR(ROUND((SELECT AVG(przydzial_myszy + NVL(myszy_extra, 0))
                                                                   FROM Kocury
                                                                   WHERE nr_bandy = K1.nr_bandy
                                                                   GROUP BY nr_bandy), 2), '99.99')
FROM Kocury K1
WHERE przydzial_myszy + NVL(myszy_extra, 0) < (SELECT AVG(przydzial_myszy + NVL(myszy_extra, 0))
                                               FROM Kocury
                                               WHERE nr_bandy = K1.nr_bandy
                                               GROUP BY nr_bandy)
    AND plec = 'M';

-- zadanie 30
-- dla kazdej bandy szukamy kotow o najdluzszym...
SELECT K.imie "IMIE", K.w_stadku_od || ' <---' "WSTAPIL DO STADKA", 'NAJSTARSZY STAZEM W BANDZIE ' || B.nazwa " "
FROM Kocury K
    LEFT JOIN Bandy B ON K.nr_bandy = B.nr_bandy
WHERE K.w_stadku_od = (SELECT MIN(K2.w_stadku_od)
                        FROM Kocury K2
                        GROUP BY K2.nr_bandy
                            HAVING K2.nr_bandy = K.nr_bandy)

UNION

-- ...i najkrotszym stazu...
SELECT K.imie "IMIE", K.w_stadku_od || ' <---' "WSTAPIL DO STADKA", 'NAJMLODSZY STAZEM W BANDZIE ' || B.nazwa " "
FROM Kocury K
    LEFT JOIN Bandy B ON K.nr_bandy = B.nr_bandy
WHERE K.w_stadku_od = (SELECT MAX(K2.w_stadku_od)
                        FROM Kocury K2
                        GROUP BY K2.nr_bandy
                            HAVING K2.nr_bandy = K.nr_bandy)

UNION 

-- ...i reszte kotow
(
SELECT K.imie "IMIE", K.w_stadku_od ||' ' "WSTAPIL DO STADKA", ' '  " "
FROM Kocury K LEFT JOIN Bandy B USING(nr_bandy)

MINUS

SELECT K.imie "IMIE", K.w_stadku_od ||' ' "WSTAPIL DO STADKA", ' '  " "
FROM Kocury K LEFT JOIN Bandy B ON K.nr_bandy = B.nr_bandy
WHERE K.w_stadku_od IN((SELECT MAX(K2.w_stadku_od)
                        FROM Kocury K2
                        GROUP BY K2.nr_bandy
                            HAVING K2.nr_bandy = K.nr_bandy),
                       (SELECT MIN(K2.w_stadku_od)
                        FROM Kocury K2
                        GROUP BY K2.nr_bandy
                            HAVING K2.nr_bandy = K.nr_bandy))
);

-- zadanie 31
CREATE VIEW Bandy_P AS
SELECT nazwa, AVG(przydzial_myszy) SRE_SPOZ, MAX(przydzial_myszy) MAX_SPOZ, MIN(przydzial_myszy) MIN_SPOZ, COUNT(pseudo) KOTY, COUNT(myszy_extra) KOTY_Z_DOD
FROM Bandy NATURAL JOIN Kocury
GROUP BY nr_bandy, nazwa;

SELECT pseudo, imie, funkcja, przydzial_myszy, 'OD ' || MIN_SPOZ || ' DO ' || MAX_SPOZ "GRANICE SPOZYCIA", w_stadku_od
FROM Kocury
    NATURAL JOIN Bandy
    NATURAL JOIN Bandy_P
WHERE pseudo = '&pseudo';

DROP VIEW BANDY_P;

-- zadanie 32
-- SELECT *
-- FROM (
--     SELECT pseudo, plec, PRZYDZIAL_MYSZY, NVL(MYSZY_EXTRA, 0)
--     FROM KOCURY NATURAL JOIN BANDY
--     WHERE NAZWA = 'CZARNI RYCERZE'
--     ORDER BY W_STADKU_OD ASC
-- )
-- WHERE ROWNUM <= 3

-- UNION

-- SELECT *
-- FROM (
--     SELECT pseudo, plec, PRZYDZIAL_MYSZY, NVL(MYSZY_EXTRA, 0)
--     FROM KOCURY NATURAL JOIN BANDY
--     WHERE NAZWA = 'LACIACI MYSLIWI'
--     ORDER BY W_STADKU_OD ASC
-- )
-- WHERE ROWNUM <= 3

SELECT * 
FROM (SELECT pseudo, plec, przydzial_myszy, NVL(myszy_extra, 0)
        FROM Kocury NATURAL JOIN Bandy
        WHERE nazwa IN ('CZARNI RYCERZE', 'LACIACI MYSLIWI')
            AND plec = 'M'
        ORDER BY w_stadku_od ASC)
WHERE ROWNUM <= 3         

UNION

SELECT * 
FROM (SELECT pseudo, plec, przydzial_myszy, NVL(myszy_extra, 0)
        FROM Kocury NATURAL JOIN Bandy
        WHERE nazwa IN ('CZARNI RYCERZE', 'LACIACI MYSLIWI')
            AND plec = 'D'
        ORDER BY w_stadku_od ASC)
WHERE ROWNUM <= 3;

UPDATE Kocury K1
-- zwieksz przydzial myszy odpowiednio dla kotek i kocurow
SET K1.przydzial_myszy = CASE 
    WHEN plec = 'M'
        THEN NVL(K1.przydzial_myszy, 0) + 10
    WHEN plec = 'D'
        THEN NVL(K1.przydzial_myszy, 0) + (0.1 * (SELECT MIN(NVL(przydzial_myszy, 0))
                                                    FROM Kocury))
    END,
    -- zwieksz przydzial myszy ekstra
    K1.myszy_extra = NVL(K1.myszy_extra, 0) + (0.15 *(SELECT AVG(NVL(K2.myszy_extra, 0))
                                                        FROM Kocury K2
                                                        WHERE K1.nr_bandy = K2.nr_bandy))
WHERE pseudo IN (SELECT *
                 FROM (SELECT pseudo
                       FROM Kocury NATURAL JOIN Bandy
                       WHERE nazwa IN ('CZARNI RYCERZE','LACIACI MYSLIWI') AND plec = 'D'
                       ORDER BY w_stadku_od)
                 WHERE ROWNUM <=3
                
                 UNION
                
                 SELECT *
                 FROM (SELECT pseudo
                       FROM Kocury NATURAL JOIN Bandy
                       WHERE nazwa IN ('CZARNI RYCERZE','LACIACI MYSLIWI') AND plec = 'M')
                 WHERE ROWNUM <=3);

SELECT * 
FROM (SELECT pseudo, plec, przydzial_myszy, NVL(myszy_extra, 0)
      FROM Kocury NATURAL JOIN Bandy
      WHERE nazwa IN ('CZARNI RYCERZE', 'LACIACI MYSLIWI')
          AND plec = 'M'
          ORDER BY w_stadku_od ASC)
WHERE ROWNUM <= 3          

UNION

SELECT * 
FROM (SELECT pseudo, plec, przydzial_myszy, NVL(myszy_extra, 0)
      FROM Kocury NATURAL JOIN Bandy
      WHERE nazwa IN ('CZARNI RYCERZE', 'LACIACI MYSLIWI')
          AND plec = 'D'
          ORDER BY w_stadku_od ASC)
WHERE ROWNUM <= 3;

ROLLBACK;

-- zadanie 33
SELECT * FROM
(
    SELECT  DECODE(plec, 'D', nazwa, ' ') "NAZWA BANDY",
            DECODE(plec, 'D', 'Kotka', 'Kocor') "PLEC",
            COUNT(pseudo) "ILE",
            SUM(DECODE(funkcja, 'SZEFUNIO', przydzial_myszy + NVL(myszy_extra,0), 0)) "SZEFUNIO",
            SUM(DECODE(funkcja, 'BANDZIOR', przydzial_myszy + NVL(myszy_extra,0), 0)) "BANDZIOR",
            SUM(DECODE(funkcja, 'LOWCZY', przydzial_myszy + NVL(myszy_extra,0), 0)) "LOWCZY",
            SUM(DECODE(funkcja, 'LAPACZ', przydzial_myszy + NVL(myszy_extra,0), 0)) "LAPACZ",
            SUM(DECODE(funkcja, 'KOT', przydzial_myszy + NVL(myszy_extra,0), 0)) "KOT",
            SUM(DECODE(funkcja, 'MILUSIA', przydzial_myszy + NVL(myszy_extra,0), 0)) "MILUSIA",
            SUM(DECODE(funkcja, 'DZIELCZY', przydzial_myszy + NVL(myszy_extra,0), 0)) "DZIELCZY",
            SUM(przydzial_myszy + NVL(myszy_extra,0)) "SUMA"
    FROM Kocury NATURAL JOIN Bandy
    GROUP BY nazwa, plec
    ORDER BY nazwa
)
UNION ALL

SELECT 'ZJADA RAZEM' , ' ', NULL,
(SELECT SUM(przydzial_myszy + NVL(myszy_extra,0)) FROM Kocury GROUP BY Funkcja HAVING funkcja = 'SZEFUNIO'),
(SELECT SUM(przydzial_myszy + NVL(myszy_extra,0)) FROM Kocury GROUP BY Funkcja HAVING funkcja = 'LOWCZY'),
(SELECT SUM(przydzial_myszy + NVL(myszy_extra,0)) FROM Kocury GROUP BY Funkcja HAVING funkcja = 'LAPACZ'),
(SELECT SUM(przydzial_myszy + NVL(myszy_extra,0)) FROM Kocury GROUP BY Funkcja HAVING funkcja = 'KOT'),
(SELECT SUM(przydzial_myszy + NVL(myszy_extra,0)) FROM Kocury GROUP BY Funkcja HAVING funkcja = 'MILUSIA'),
(SELECT SUM(przydzial_myszy + NVL(myszy_extra,0)) FROM Kocury GROUP BY Funkcja HAVING funkcja = 'BANDZIOR'),
(SELECT SUM(przydzial_myszy + NVL(myszy_extra,0)) FROM Kocury GROUP BY Fun HAVING funkcja = 'DZIELCZY'),
(SELECT SUM(przydzial_myszy + NVL(myszy_extra,0)) FROM Kocury)
FROM Dual;


-- b
SELECT  DECODE(plec, 'D', nazwa, NULL, 'ZJADA RAZEM', ' ') "Nazwa bandy",
        DECODE(plec, 'M', 'Kocur', 'D', 'Kotka') "Plec",
        ILE,
        NVL(SZEFUNIO, 0) SZEFUNIO,
        NVL(BANDZIOR, 0) BANDZIOR,
        NVL(LOWCZY, 0) LOWCZY,
        NVL(LAPACZ, 0) LAPACZ,
        NVL(KOT, 0) KOT,
        NVL(MILUSIA, 0) MILUSIA,
        NVL(DZIELCZY, 0) DZIELCZY,
        SUMA
FROM (
    SELECT *
    FROM (
        SELECT * 
        FROM (    
            SELECT nazwa, plec, NVL(funkcja, 'SUMA') fun, SUM(przydzial_myszy + NVL(myszy_extra, 0)) myszy_calk
            FROM Kocury
                NATURAL JOIN Bandy
            GROUP BY GROUPING SETS(                
                -- suma przydzialow w bandzie dla kazdej funkcji ze wzgledu na plec
                (nazwa, plec, funkcja),
                
                -- suma przydzialow w bandzie ze wzgledu na plec (pseudo funkcja 'SUMA')
                (nazwa, plec),
                
                -- suma przydzialow w funkcji
                (funkcja),
                
                -- suma przydzialow w stadku
                ()
            )
        )
        WHERE plec IS NOT NULL AND nazwa IS NOT NULL OR (plec IS NULL AND nazwa IS NULL)
    )
    PIVOT (
        SUM(myszy_calk)
        FOR fun
        IN (
            'SZEFUNIO' SZEFUNIO,
            'BANDZIOR' BANDZIOR,
            'LOWCZY' LOWCZY,
            'LAPACZ' LAPACZ,
            'KOT' KOT,
            'MILUSIA' MILUSIA,
            'DZIELCZY' DZIELCZY,
            'SUMA' SUMA)
    )
)
    LEFT JOIN (
        SELECT nazwa, plec, COUNT(pseudo) ILE
        FROM Kocury
            NATURAL JOIN Bandy
        GROUP BY nazwa, plec)
    USING(nazwa, plec)
ORDER BY nazwa, plec;

    
