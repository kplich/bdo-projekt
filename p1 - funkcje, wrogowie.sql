CREATE TABLE Funkcje(
    funkcja VARCHAR2(10) CONSTRAINT f_funkcja_pk PRIMARY KEY,
    min_myszy NUMBER(3) CONSTRAINT f_min_gt5 CHECK(min_myszy > 5),
    max_myszy NUMBER(3) CONSTRAINT f_lt200 CHECK(max_myszy > 200),
    CONSTRAINT f_max_lt_min CHECK(max_myszy >= min_myszy)
);
 
CREATE TABLE Wrogowie(
    imie_wroga VARCHAR(15) CONSTRAINT w_imie_pk PRIMARY KEY,
    stopien_wrogosci NUMBER(2),
    gatunek VARCHAR2(15),
    lapowka VARCHAR2(20),
    CONSTRAINT w_stopien_1_10 CHECK(stopien_wrogosci BETWEEN 1 AND 10)
);

CREATE TABLE Bandy(
    nr_bandy NUMBER(2) CONSTRAINT b_nr_pk PRIMARY KEY,
    nazwa VARCHAR2(20) CONSTRAINT b_nazwa_nn NOT NULL,
    teren VARCHAR2(15) CONSTRAINT b_teren_unq UNIQUE,
    szef_bandy VARCHAR2(15) CONSTRAINT b_szefb_unq UNIQUE
);

CREATE TABLE Kocury(
    imie VARCHAR2(15) CONSTRAINT k_imie_nn NOT NULL,
    plec VARCHAR2(1) CONSTRAINT k_plec_m_d CHECK(plec IN ('M', 'D')),
    pseudo VARCHAR2(15) CONSTRAINT k_pseudo_pk PRIMARY KEY,
    funkcja VARCHAR2(10) CONSTRAINT k_fun_fk REFERENCES Funkcje(funkcja),
    szef VARCHAR2(15) CONSTRAINT k_szef_fk REFERENCES Kocury(pseudo),
    w_stadku_od DATE DEFAULT SYSDATE,
    przydzial_myszy NUMBER(3),
    myszy_extra NUMBER(3),
    nr_bandy NUMBER(2) CONSTRAINT k_nrb_fk REFERENCES Bandy(nr_bandy)
);

ALTER TABLE Bandy
    ADD CONSTRAINT b_szef_fk FOREIGN KEY(szef_bandy) REFERENCES Kocury(pseudo);

CREATE TABLE Wrogowie_Kocurow(
    pseudo VARCHAR2(15) CONSTRAINT wk_pseudo_fk REFERENCES Kocury(pseudo),
    imie_wroga VARCHAR2(15) CONSTRAINT wk_imiew_fk REFERENCES Wrogowie(imie_wroga),
    data_incydentu DATE CONSTRAINT wk_data_nn NOT NULL,
    opis_incydentu VARCHAR2(50),
    CONSTRAINT wk_pk PRIMARY KEY(pseudo, imie_wroga)
);

